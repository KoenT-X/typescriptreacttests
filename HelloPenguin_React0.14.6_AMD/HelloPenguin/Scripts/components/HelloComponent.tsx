﻿/// <reference path="../typings/react/react.d.ts" />

interface HelloComponentProps
{
    userName: string;
}

interface HelloComponentState
{
    disabled: boolean;
}

class HelloComponent extends React.Component<HelloComponentProps, HelloComponentState>
{
    constructor(props: HelloComponentProps)
    {
        super(props);
        this.state = { disabled: false };
    }

    render(): JSX.Element
    {
        return (
            <div>
                <label>The following is a hello greeting:</label>
                <div>Hello, {this.props.userName}!</div>
            </div>
        );
    }

    static get defaultProps(): HelloComponentProps
    {
        return { userName: "Dummy" };
    }
}

export = HelloComponent;
