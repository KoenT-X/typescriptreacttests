﻿/// <reference path="../typings/react/react.d.ts" />

interface TimerComponentProps
{
    updateIntervalInMs: number;
}

interface TimerComponentState
{
    elapsedTimeInSeconds: number;
}

class TimerComponent extends React.Component<TimerComponentProps, TimerComponentState>
{
    constructor(props: TimerComponentProps)
    {
        super(props);
        this.state = { elapsedTimeInSeconds: 0.0 };
    }

    componentDidMount()
    {
        this.intervalTimerID = setInterval(() => this.tick(), this.props.updateIntervalInMs);
    }

    componentWillUnmount()
    {
        clearInterval(this.intervalTimerID);
    }

    render(): JSX.Element
    {
        return <div>Seconds elapsed: {this.state.elapsedTimeInSeconds}</div>;
    }

    tick()
    {
        this.setState({ elapsedTimeInSeconds: this.state.elapsedTimeInSeconds + this.props.updateIntervalInMs / 1000 });
    }

    static get defaultProps(): TimerComponentProps
    {
        return { updateIntervalInMs: 100 };
    }

    private intervalTimerID: number;
}

export = TimerComponent;
