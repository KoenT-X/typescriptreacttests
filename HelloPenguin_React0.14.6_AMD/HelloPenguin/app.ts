﻿/// <reference path="Scripts/typings/react/react.d.ts" />
/// <reference path="Scripts/typings/react/react-dom.d.ts" />

import TimerComponent = require("Scripts/components/TimerComponent");
import HelloComponent = require("Scripts/components/HelloComponent");
import ApplicationView = require("Scripts/components/ApplicationView");

var timerComponent = React.createElement(TimerComponent);
ReactDOM.render(timerComponent, document.getElementById("timerComponent"));

var helloComponent = React.createElement(HelloComponent);
ReactDOM.render(helloComponent, document.getElementById("helloComponent"));

var app = React.createElement(ApplicationView);
ReactDOM.render(app, document.getElementById("applicationView"));

console.log("End of app.ts reached.");
