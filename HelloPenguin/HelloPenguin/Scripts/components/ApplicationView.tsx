﻿/// <reference path="../typings/react/react.d.ts" />

module Penguin
{

    interface ApplicationViewProps
    {

    }

    interface ApplicationViewState
    {
        text: string;
    }

    export class ApplicationView extends React.Component<ApplicationViewProps, ApplicationViewState>
    {
        constructor(props: ApplicationViewProps)
        {
            super(props);
            this.state = ApplicationView.makeStateFromText("John Doe");
        }

        componentDidMount()
        {
            // add listener to store
        }

        componentWillUnmount()
        {
            // remove store listener
        }

        render(): JSX.Element
        {
            return <div>
                        <label>Type a name: </label>
                        <input type="text" value={this.state.text} onChange={ (event) => this.onNameTextInputChanged(event) }/>
                        <br/>
                        <label>Hello, {this.state.text}!</label>
                        <TimerComponent updateIntervalInMs={500} />
                        <HelloComponent userName="Stranger" />
                </div>
        }

        onNameTextInputChanged(event)
        {
            let input = event.target as HTMLInputElement;
            this.setState(ApplicationView.makeStateFromText(input.value));
        }

        static makeStateFromText(someText: string): ApplicationViewState
        {
            return { text: someText };
        }
    }

}
