﻿/// <reference path="components/ApplicationView.tsx" />

module Penguin.App {
    if (window != null && window != undefined) {
        window.onload = function () {
            var timerComponent = React.createElement(Penguin.TimerComponent);
            React.render(timerComponent, document.getElementById("timerComponent"));

            var helloComponent = React.createElement(Penguin.HelloComponent);
            React.render(helloComponent, document.getElementById("helloComponent"));

            var app = React.createElement(Penguin.ApplicationView);
            React.render(app, document.getElementById("applicationView"));
        }
    }
}
