﻿/// <reference path="components/ApplicationView.react.tsx" />

module Penguin.App {
    if (window != null && window != undefined) {
        window.onload = function () {
            var app = React.createElement(Penguin.ApplicationView);
            React.render(app, document.getElementById("penguinApp"));
        }
    }
}

