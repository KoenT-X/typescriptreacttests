﻿/// <reference path="../typings/react/react.d.ts" />
/// <reference path="../EventEmitter.ts" />

// -- note: the order of the references may determine in which order events are handled -- //

module Penguin.NoteStore {

    export interface Note {
        text: string;
        date: Date;
    }

    var CHANGE_EVENT: string = "change";
    var state = {
        emitter: new EventEmitter(),
        items: new Array<Note>(),
    }

    export function getNotes(): Note[] {

        // copy items so state cannot be changed in the object store
        var items = new Array<Note>(state.items.length);
        for (let i in state.items) {
            let item = state.items[i];
            items.push({text: item.text, date: item.date });
        }
        return items;
    }

    export function addChangeListener(callback: Function) {
        state.emitter.addListener(CHANGE_EVENT, callback);
    }

    export function removeChangeListener(callback: Function) {
        state.emitter.removeListener(CHANGE_EVENT, callback);
    }

    export function addItem(text: string): void {
        state.items.push({ text: text, date: new Date() });
        state.emitter.emit(CHANGE_EVENT);
    }
}