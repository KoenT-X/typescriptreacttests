﻿/// <reference path="../typings/react/react.d.ts" />
/// <reference path="../stores/NoteStore.ts" />

// -- note: the order of the references may determine in which order events are handled -- //

module Penguin {

	interface State {
		text: string;
		count: number;
		items: NoteStore.Note[];
	}

	export class ApplicationView extends React.Component<any, State> {

		private textInput: any;

		constructor()
		{
			super();
			this.textInput = null;
			this._onStoreUpdated = this._onStoreUpdated.bind(this);
			this._onChange = this._onChange.bind(this);
			this._onAdd = this._onAdd.bind(this);
			this.state = getState("some text");
			NoteStore.addChangeListener(this._onStoreUpdated);
		}

		public render(): JSX.Element {
			var state = this.state as State;

			var items = new Array<any>();
			
			for (var i in state.items) {
				var item = state.items[i];
				var view = <li key={i}>{item.text} - {item.date.toLocaleTimeString()}</li>;
				items.push(view);
			}

			return <div>
                        <input type="text" value={state.text} ref={function (x) { this.textInput = x; } } onChange={this._onChange}/>
						<button value="add" onClick={this._onAdd}>add</button>
						<br/>
						<label>{state.text}</label>
						<ul>
							{items}
						</ul>
					</div>;
		}

		private _onStoreUpdated(): void {
            this.setState(getState("some text"));
		}
		
		private _onChange(e: React.FormEvent): void {
			var input = e.target as HTMLInputElement;
			var newState = getState(input.value);
			this.setState(newState);
		}

		private _onAdd(e: React.FormEvent): void {
			var state = this.state;
			NoteStore.addItem(state.text);
		}
	}

	function getState(text: string): State {

		var notes = NoteStore.getNotes();
		return {
			text: text,
			count: notes.length,
			items: notes,
		};
	}
}