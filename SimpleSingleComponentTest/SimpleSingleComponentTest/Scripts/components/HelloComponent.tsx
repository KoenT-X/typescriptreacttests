﻿/// <reference path="../typings/react/react.d.ts" />

interface IHelloComponentProps
{
    userName: string;
}

interface IHelloComponentState
{
    disabled: boolean;
}

class HelloComponent extends React.Component<IHelloComponentProps, IHelloComponentState>
{
    constructor(props: IHelloComponentProps)
    {
        super(props);
        this.state = { disabled: false };
    }

    public render(): JSX.Element
    {
        return (
            <div>
                <label>A dummy label.</label>
                <div>Hello, {this.props.userName}!</div>
            </div>
        );
    }
}

export = HelloComponent;
