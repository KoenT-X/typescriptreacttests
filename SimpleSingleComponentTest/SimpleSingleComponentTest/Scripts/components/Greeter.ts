﻿// Just the default TypeScript Greeter class that VS2015 generates (but getting the document from the element).
// Not related in any way to React.

class Greeter
{
    element: HTMLElement;
    span: HTMLElement;
    timerToken: number;

    constructor(element: HTMLElement)
    {
        this.element = element;
        this.element.innerHTML += "The time is: ";
        this.span = element.ownerDocument.createElement("span");
        this.element.appendChild(this.span);
        this.span.innerText = new Date().toUTCString();
    }

    start()
    {
        this.timerToken = setInterval(() => this.span.innerHTML = new Date().toUTCString(), 500);
    }

    stop()
    {
        clearTimeout(this.timerToken);
    }
}

export = Greeter;
