﻿/// <reference path="Scripts/typings/react/react.d.ts" />
/// <reference path="Scripts/typings/react/react-dom.d.ts" />

import Greeter = require("Scripts/components/Greeter");
import HelloComponent = require("Scripts/components/HelloComponent");

var greeter = new Greeter(document.getElementById("greeter"));
greeter.start();

var helloComponent = React.createElement(HelloComponent, { userName: "John Doe" });
ReactDOM.render(helloComponent, document.getElementById("helloComponent"));

console.log("log reached");
