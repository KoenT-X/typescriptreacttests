Test projects using TypeScript, React, and the AMD module system in Visual Studio 2015.

HelloPenguin:
Project using older version of React 0.13.x and no AMD module system.

HelloPenguin_React0.14.6_AMD:
Same project as HelloPenguin, but with recent React version and AMD module system.

HelloPenguin2: 
Project using older version of React 0.13.x and no AMD module system; more elaborated, using a store and event emitter.

HelloPenguin2_React0.14.6_AMD:
Same project as HelloPenguin2, but with recent React version and AMD module system.

SimpleSingleComponentTest:
Very simple React test with only a single component, no nesting, no store.
