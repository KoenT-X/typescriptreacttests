﻿/// <reference path="Scripts/typings/react/react.d.ts" />
/// <reference path="Scripts/typings/react/react-dom.d.ts" />

import ApplicationView = require("Scripts/components/ApplicationView");

let app = React.createElement(ApplicationView);
ReactDOM.render(app, document.getElementById("applicationView"));

console.log("End of app.ts reached.");
