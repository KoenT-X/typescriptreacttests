﻿/// <reference path="../typings/react/react.d.ts" />

import EventEmitter = require("../EventEmitter");

// Note: converted original set of functions into class with only static methods.
// It would probably be better to make it so that multiple stores are possible,
// by making this a normal class with normal methods, and then instantiating a store
// for each of the views that need one.

class NoteStore
{
    static CHANGE_EVENT: string = "change";

    static addItem(text: string): void
    {
        this.state.items.push({ text: text, date: new Date() });
        this.state.emitter.emit(NoteStore.CHANGE_EVENT);
    }

    static getNotes(): NoteStore.Note[]
    {
        // Copy items so state cannot be changed in the object store
        let items = new Array<NoteStore.Note>(this.state.items.length);
        for (let i in this.state.items)
        {
            let item = this.state.items[i];
            items.push({ text: item.text, date: item.date });
        }
        return items;
    }

    static addChangeListener(callback: Function)
    {
        this.state.emitter.addListener(NoteStore.CHANGE_EVENT, callback);
    }

    static removeChangeListener(callback: Function)
    {
        this.state.emitter.removeListener(NoteStore.CHANGE_EVENT, callback);
    }

    private static state =
    {
        emitter: new EventEmitter(),
        items: new Array<NoteStore.Note>()
    }
}

module NoteStore
{
    export interface Note
    {
        text: string;
        date: Date;
    }
}

export = NoteStore;
