﻿/// <reference path="../typings/react/react.d.ts" />

import NoteStore = require("../stores/NoteStore");

interface ApplicationViewProps
{

}

interface ApplicationViewState
{
    text: string;
    count: number;
    items: NoteStore.Note[];
}

class ApplicationView extends React.Component<ApplicationViewProps, ApplicationViewState>
{
    constructor(props: ApplicationViewProps)
    {
        super();
        this.state = this.makeStateFromText("some text");
        NoteStore.addChangeListener(() => this.onStoreUpdated());
    }

    public render(): JSX.Element
    {
        let state = this.state as ApplicationViewState;

        // Create array of items for list view
        let listViewItems = new Array<any>();
        for (let i in state.items)
        {
            let item = state.items[i];
            let view = <li key={i}>{item.text} - {item.date.toLocaleTimeString() }</li>;
            listViewItems.push(view);
        }

        return <div>
                    <input type="text" value={state.text} onChange={(e) => this.onTextInputChange(e)}/>
                    <button value="add" onClick={(e) => this.onAddButtonClick(e)}>add</button>
                    <br/>
                    <label>{state.text}</label>
                    <ul>
                        {listViewItems}
                    </ul>
            </div>;
    }

    private onStoreUpdated(): void
    {
        this.setState(this.makeStateFromText("some text"));
    }

    private onTextInputChange(e: React.FormEvent): void
    {
        let input = e.target as HTMLInputElement;
        let newState = this.makeStateFromText(input.value);
        this.setState(newState);
    }

    private onAddButtonClick(e: React.FormEvent): void
    {
        let state = this.state;
        NoteStore.addItem(state.text);
    }

    private makeStateFromText(someText: string): ApplicationViewState 
    {
        let notes = NoteStore.getNotes();
        return {
            text: someText,
            count: notes.length,
            items: notes
        };
    }
}

export = ApplicationView;
